var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.Pitch', function() {
    it('Pitch exists', function() {
        expect(Music.Pitch).to.exist;
    });
    
    var valueToLetter = {
        0: 'C',
        1: 'C#',
        2: 'D',
        3: 'D#',
        4: 'E',
        5: 'F',
        6: 'F#',
        7: 'G',
        8: 'G#',
        9: 'A',
        10: 'A#',
        11: 'B',
    };
    
    it('Returns the right letters', function() {
        for (pitchValue in valueToLetter) {
            var pitch = new Music.Pitch(pitchValue);
            expect(pitch.getLetter()).to.equal(valueToLetter[pitchValue]);
        }
    });
    
    it('Returns the right values', function() {
        for (pitchValue in valueToLetter) {
            var pitch = new Music.Pitch(valueToLetter[pitchValue]);
            expect(pitch.getValue()).to.equal(parseInt(pitchValue, 10));
        }
    });
    
    it('C 4 is 48', function() {
        var pitch = new Music.Pitch('C', 4);
        expect(pitch.getValue()).to.equal(48);
    });
});