var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.Rhythm', function() {
    it('Rhythm exists', function() {
        expect(Music.Rhythm).to.exist;
    });
    
    it('Can create instance', function() {
        var timeSignature = new Music.TimeSignature(4, 4);
        var rhythm = new Music.Rhythm(timeSignature);
    });
    
    it('Can add to rhythm', function() {
        var timeSignature = new Music.TimeSignature(4, 4);
        var rhythm = new Music.Rhythm(timeSignature);
        
        for (var i=0; i<8; i++) {
            rhythm.add(new Music.NoteDuration(1, 8));
        }
        
        for (var i=0; i<rhythm.length; i++) {
            expect(rhythm[i]).to.exist;
            expect(rhythm[i].size).to.exist;
            expect(rhythm[i].soundFor).to.exist;
            expect(rhythm[i].size.getTicks()).to.equal(48);
            expect(rhythm[i].soundFor.getTicks()).to.equal(48);
        }
    });
    
    
});