var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../../music.js');

describe('Music.Util.Random', function() {
    it('Random generator exists', function() {
        expect(Music.Util.Random).to.exist;
    });
    
    it('always output the same number for a given seed', function() {
        var random = new Music.Util.Random('Hello Testing!');
        expect(Math.floor(random.getNumber() * 1000)).to.equal(794);
        
        random = new Music.Util.Random('Another Seed');
        expect(Math.floor(random.getNumber() * 1000)).to.equal(19);
    });
    
    it('getNumberBetween returns a number between specified values', function() {
        var random = new Music.Util.Random('Test Helloing!');
        
        for (var i=0; i<1000; i++) {
            var number = random.getNumberBetween(5, 12);
            expect(number).to.be.above(4);
            expect(number).to.be.below(13);
        }
    });
    
    it('arrayRandomChild returns a random child from an array', function() {
        var random = new Music.Util.Random('Harrow Testing!');
        var testArray = ['one', 'two', 'three', 'four'];
        expect(random.arrayRandomChild(testArray)).to.equal('two');
        expect(random.arrayRandomChild(testArray)).to.equal('two');
        expect(random.arrayRandomChild(testArray)).to.equal('four');
        expect(random.arrayRandomChild(testArray)).to.equal('three');
        expect(random.arrayRandomChild(testArray)).to.equal('three');
        expect(random.arrayRandomChild(testArray)).to.equal('four');
        expect(random.arrayRandomChild(testArray)).to.equal('two');
        expect(random.arrayRandomChild(testArray)).to.equal('one');
    });
    
    it('objectRandomPropertyKey should return a random key from an object', function() {
        var random = new Music.Util.Random('Tello Hesting!');
        var testObject = {
            "member1": "",
            "member2": "",
            "member3": "",
            "member4": ""
        };
        
        expect(random.objectRandomPropertyKey(testObject)).to.equal('member3');
        expect(random.objectRandomPropertyKey(testObject)).to.equal('member4');
        expect(random.objectRandomPropertyKey(testObject)).to.equal('member1');
        expect(random.objectRandomPropertyKey(testObject)).to.equal('member4');
        expect(random.objectRandomPropertyKey(testObject)).to.equal('member2');
    });
    
    it('arrayAssignRandom should assign value to a random member of the array', function() {
        var random = new Music.Util.Random('A harrowing tale of testing');
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 9);
        assert.deepEqual(testArray, [0, 0, 0, 9]);
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 4);
        assert.deepEqual(testArray, [0, 0, 0, 4]);
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 9);
        assert.deepEqual(testArray, [9, 0, 0, 0]);
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 9);
        assert.deepEqual(testArray, [0, 0, 0, 9]);
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 5);
        assert.deepEqual(testArray, [0, 5, 0, 0]);
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 9);
        assert.deepEqual(testArray, [0, 9, 0, 0]);
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 9);
        assert.deepEqual(testArray, [9, 0, 0, 0]);
        
        var testArray = [0, 0, 0, 0];
        random.arrayAssignRandom(testArray, 9);
        assert.deepEqual(testArray, [0, 0, 0, 9]);
    });
});
