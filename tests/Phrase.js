var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.Phrase', function() {
    it('Phrase exists', function() {
        expect(Music.Phrase).to.exist;
    });
    
    it('addItem() works', function() {
        var phrase = new Music.Phrase();
        var note = new Music.Note(new Music.Pitch('C', 4), new Music.NoteDuration(1, 4));
        phrase.add(new Music.NoteDuration(1, 4), [note]);
        expect(phrase[0].notes[0]).to.equal(note);
        expect(phrase[0].duration.getTicks()).to.equal(96);
    });
    
    it('getNoteEvents() 4 quarter single notes', function() {
        var phrase = new Music.Phrase();
        var duration = new Music.NoteDuration(1, 4);
        var pitch = new Music.Pitch('C', 4);
        var note = new Music.Note(pitch, duration);
        
        phrase.add(duration, [note]);
        phrase.add(duration, [note]);
        phrase.add(duration, [note]);
        phrase.add(duration, [note]);
        var noteEvents = phrase.getNoteEvents();
        
        expect(noteEvents.length).to.equal(8);
        expect(noteEvents[0].delta).to.equal(0);
        expect(noteEvents[1].delta).to.equal(96);
        expect(noteEvents[2].delta).to.equal(0);
        expect(noteEvents[3].delta).to.equal(96);
        expect(noteEvents[4].delta).to.equal(0);
        expect(noteEvents[5].delta).to.equal(96);
        expect(noteEvents[6].delta).to.equal(0);
        expect(noteEvents[7].delta).to.equal(96);
    });
    
    it('getNoteEvents() 4 eigth single notes', function() {
        var phrase = new Music.Phrase();
        var quarter = new Music.NoteDuration(1, 4);
        var eighth = new Music.NoteDuration(1, 8);
        var pitch = new Music.Pitch('C', 4);
        var note = new Music.Note(pitch, eighth);
        
        phrase.add(quarter, [note]);
        phrase.add(quarter, [note]);
        phrase.add(quarter, [note]);
        phrase.add(quarter, [note]);
        
        var noteEvents = phrase.getNoteEvents();
        
        expect(noteEvents.length).to.equal(8);
        expect(noteEvents[0].delta).to.equal(0);
        expect(noteEvents[1].delta).to.equal(48);
        expect(noteEvents[2].delta).to.equal(48);
        expect(noteEvents[3].delta).to.equal(48);
        expect(noteEvents[4].delta).to.equal(48);
        expect(noteEvents[5].delta).to.equal(48);
        expect(noteEvents[6].delta).to.equal(48);
        expect(noteEvents[7].delta).to.equal(48);
    });
});