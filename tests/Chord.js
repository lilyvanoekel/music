var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.Chord', function() {
    it('Chord exists', function() {
        expect(Music.Chord).to.exist;
    });
    
    it('returns pitches', function() {
        var chord = new Music.Chord(
            new Music.ChordType('major'),
            new Music.Pitch('C')
        );
        
        var pitches = chord.getPitches().join(',');
        expect(pitches).to.equal('C,E,G');
    });
    
    it('toString works', function() {
        var chord = new Music.Chord(
            new Music.ChordType('major7'),
            new Music.Pitch('C')
        );
        
        expect('' + chord).to.equal('Cmaj7');
    });
});