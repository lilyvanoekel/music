var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.Scale', function() {
    it('Scale exists', function() {
        expect(Music.Scale).to.exist;
    });
    
    it('returns pitches', function() {
        var scale = new Music.Scale(
            new Music.ScaleType('major'),
            new Music.Pitch('C')
        );
        
        var pitches = scale.getPitches().join(',');
        expect(pitches).to.equal('C,D,E,F,G,A,B')
    });
    
    it('getPitchByNumeral works', function() {
        var scale = new Music.Scale(
            new Music.ScaleType('major'),
            new Music.Pitch('A')
        );
        
        expect(scale.getPitchByNumeral(1).getLetter()).to.equal('A');
        expect(scale.getPitchByNumeral(2).getLetter()).to.equal('B');
        expect(scale.getPitchByNumeral(3).getLetter()).to.equal('C#');
        expect(scale.getPitchByNumeral(4).getLetter()).to.equal('D');
        expect(scale.getPitchByNumeral(5).getLetter()).to.equal('E');
        expect(scale.getPitchByNumeral(6).getLetter()).to.equal('F#');
        expect(scale.getPitchByNumeral(7).getLetter()).to.equal('G#');
        expect(scale.getPitchByNumeral(8).getLetter()).to.equal('A');
        
        var scale = new Music.Scale(
            new Music.ScaleType('minor'),
            new Music.Pitch('D')
        );
        
        expect(scale.getPitchByNumeral(1).getLetter()).to.equal('D');
        expect(scale.getPitchByNumeral(2).getLetter()).to.equal('E');
        expect(scale.getPitchByNumeral(3).getLetter()).to.equal('F');
        expect(scale.getPitchByNumeral(4).getLetter()).to.equal('G');
        expect(scale.getPitchByNumeral(5).getLetter()).to.equal('A');
        expect(scale.getPitchByNumeral(6).getLetter()).to.equal('A#');
        expect(scale.getPitchByNumeral(7).getLetter()).to.equal('C');
        expect(scale.getPitchByNumeral(8).getLetter()).to.equal('D');
    });
});