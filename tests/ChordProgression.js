var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.ChordProgression', function() {
    it('exists', function() {
        expect(Music.ChordProgression).to.exist;
    });
    
    it('Can add to progression', function() {
        var progression = new Music.ChordProgression();
        
        var chord = new Music.Chord(
            new Music.ChordType('major'),
            new Music.Pitch('C')
        );
        
        var duration = new Music.NoteDuration(1, 1);
        
        for (var i=0; i<8; i++) {
            progression.add(chord, duration);
        }
        
        for (var i=0; i<progression.length; i++) {
            expect(progression[i]).to.exist;
            expect(progression[i].chord).to.exist;
            expect(progression[i].duration).to.exist;
            expect(progression[i].chord.getName()).to.equal('major');
        }
    });
    
});