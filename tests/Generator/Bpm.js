var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../../music.js');

describe('Music.Generator.Bpm', function() {
    it('exists', function() {
        expect(Music.Generator.Bpm).to.exist;
    });
    
    it('takes random in constructor', function() {
        var random = new Music.Util.Random('hello');
        var bpmGenerator = new Music.Generator.Bpm(random);
        expect(bpmGenerator.random).to.exist;
    });
    
    it('returns bpm between min and max', function() {
        var random = new Music.Util.Random('hello');
        var bpmGenerator = new Music.Generator.Bpm(random);
        bpmGenerator.min = 100;
        bpmGenerator.max = 140;
        
        for (var i=0; i<1000; i++) {
            var bpm = bpmGenerator.generate();
            expect(bpm).to.exist;
            expect(bpm.bpm).to.be.above(99);
            expect(bpm.bpm).to.be.below(141);
        }
    });
    
});