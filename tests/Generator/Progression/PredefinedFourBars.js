var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../../../music.js');

describe('Music.Generator.Progression.PredefinedFourBars', function() {
    it('exists', function() {
        expect(Music.Generator.Progression.PredefinedFourBars).to.exist;
    });
    
    it('throws error when no scale present', function() {
        expect(function() {
            var generator = new Music.Generator.Progression.PredefinedFourBars(
                new Music.Util.Random('hello')
            );
            generator.generate();
        }).to.throw(Error);
    });
    
    it('returns progression', function() {
        var generator = new Music.Generator.Progression.PredefinedFourBars(
            new Music.Util.Random('hello')
        );
        
        generator.scale = new Music.Scale(
            new Music.ScaleType('major'),
            new Music.Pitch('C')
        );
        
        var progression = generator.generate();
        assert.instanceOf(progression, Music.ChordProgression);
    });
    
    it('always returns progression with 4 valid chords', function() {
        var random = new Music.Util.Random('asda1232342342342341');
        
        var generator = new Music.Generator.Progression.PredefinedFourBars(
            random
        );
        
        for (var i=0; i<100; i++) {
            generator.scale = new Music.Scale(
                new Music.ScaleType('major'),
                new Music.Pitch('C')
            );
            
            var progression = generator.generate();
            expect(progression.length).to.equal(4);
            
            assert.instanceOf(progression[0].chord, Music.Chord);
            assert.instanceOf(progression[1].chord, Music.Chord);
            assert.instanceOf(progression[2].chord, Music.Chord);
            assert.instanceOf(progression[3].chord, Music.Chord);
            
            assert.instanceOf(progression[0].duration, Music.NoteDuration);
            assert.instanceOf(progression[1].duration, Music.NoteDuration);
            assert.instanceOf(progression[2].duration, Music.NoteDuration);
            assert.instanceOf(progression[3].duration, Music.NoteDuration);
        }
    });
});