var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../../music.js');

describe('Music.Generator.Scale', function() {
    it('exists', function() {
        expect(Music.Generator.Scale).to.exist;
    });
    
    it('outputs a scale', function() {
        var random = new Music.Util.Random('hello');
        var scaleGenerator = new Music.Generator.Scale(random);
        var scale = scaleGenerator.generate();
        assert.instanceOf(scale, Music.Scale);
    });
    
    it('returns random type and pitch', function() {
        var random = new Music.Util.Random('helloo');
        var scaleGenerator = new Music.Generator.Scale(random);
        scaleGenerator.types = ['major', 'minor', 'minor_harmonic'];
        
        var scale = scaleGenerator.generate();
        expect(scale.type.getName()).to.equal('minor_harmonic');
        expect(scale.pitch.getLetter()).to.equal('F');
        
        var scale = scaleGenerator.generate();
        expect(scale.type.getName()).to.equal('minor');
        expect(scale.pitch.getLetter()).to.equal('D');
        
        var scale = scaleGenerator.generate();
        expect(scale.type.getName()).to.equal('minor_harmonic');
        expect(scale.pitch.getLetter()).to.equal('C#');
        
        var scale = scaleGenerator.generate();
        expect(scale.type.getName()).to.equal('minor');
        expect(scale.pitch.getLetter()).to.equal('G');
        
        var expected = [
            'minor',
            'minor',
            'minor',
            'major',
            'minor_harmonic',
            'minor_harmonic',
            'minor_harmonic',
            'major',
            'minor_harmonic',
            'minor'
        ];
        
        for (var i=0; i<10; i++) {
            var scale = scaleGenerator.generate();
            expect(scale.type.getName()).to.equal(expected[i]);
        }
    });
    
    it('returns only scale types that were chosen', function() {
        var random = new Music.Util.Random('helloo');
        var scaleGenerator = new Music.Generator.Scale(random);
        scaleGenerator.types = ['major', 'minor'];
        
        for (var i=0; i<100; i++) {
            var scale = scaleGenerator.generate();
            expect(scale.type.getName()).to.be.oneOf(['minor', 'major']);
        }
    });
    
    it('returns only pitch that were chosen', function() {
        var random = new Music.Util.Random('helloo');
        var scaleGenerator = new Music.Generator.Scale(random);
        scaleGenerator.pitch = ['C', 4, 'A'];
        var letters = {};
        
        for (var i=0; i<100; i++) {
            var scale = scaleGenerator.generate();
            expect(scale.pitch.getLetter()).to.be.oneOf(['C', 'E', 'A']);
            letters[scale.pitch.getLetter()] = true;
        }
        
        expect(letters['C']).to.exist;
        expect(letters['E']).to.exist;
        expect(letters['A']).to.exist;
    });
    
    it('supports assigning single values', function() {
        var random = new Music.Util.Random('helloo');
        var scaleGenerator = new Music.Generator.Scale(random);
        scaleGenerator.pitch = 'C';
        scaleGenerator.types = 'major';
        
        for (var i=0; i<100; i++) {
            var scale = scaleGenerator.generate();
            expect(scale.pitch.getLetter()).to.equal('C');
            expect(scale.type.getName()).to.equal('major');
        }
    });
});