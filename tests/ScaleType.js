var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.ScaleType', function() {
    it('ScaleType exists', function() {
        expect(Music.ScaleType).to.exist;
    });
    
    var scalesToTest = {
        'major': {
            'C': 'C,D,E,F,G,A,B',
            'B': 'B,C#,D#,E,F#,G#,A#'
        },
        'minor': {
            'A': 'A,B,C,D,E,F,G',
            'F': 'F,G,G#,A#,C,C#,D#'
        },
        'minor_harmonic': {
            'A': 'A,B,C,D,E,F,G#',
            'F': 'F,G,G#,A#,C,C#,E'
        }
    };
    
    for (var scaleName in scalesToTest) {
        it('should define "' + scaleName + '" scale', function() {
            var scaleType = new Music.ScaleType(scaleName);
        });
        
        for (var letter in scalesToTest[scaleName]) {
            it('should output ' + scalesToTest[scaleName][letter] + ' for ' + letter + ' ' + scaleName, function() {
                var scaleType = new Music.ScaleType(scaleName);
                var pitch = new Music.Pitch(letter);
                var scale = new Music.Scale(scaleType, pitch);
                var pitches = scale.getPitches();
                expect(pitches.join(',')).to.equal(scalesToTest[scaleName][letter]);
            });
        };
    };
    
});