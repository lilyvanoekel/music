var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.ChordType', function() {
    it('ChordType exists', function() {
        expect(Music.ChordType).to.exist;
    });
    
    it('Throws an error for unsupported chord types', function() {
        expect(function() {
            var type = new Music.ChordType('asdads');
        }).to.throw(Error);
    });
    
    it('Does not throw error for supported chord type', function() {
        var type = new Music.ChordType('major');
    });
    
    var chordsToTest = {
        major: 'C,E,G',
        minor: 'C,D#,G',
        diminished: 'C,D#,F#',
        augmented: 'C,E,G#',
        suspended4: 'C,F,G',
        major7: 'C,E,G,B',
        minor7: 'C,D#,G,A#',
        dominant7: 'C,E,G,A#',
        diminished7: 'C,D#,F#,A',
        augmented_major7: 'C,E,G#,B',
        augmented_minor7: 'C,E,G#,A#',
        half_diminished7: 'C,D#,F#,A#'
    };
    
    for (var chordName in chordsToTest) {
        it('defines chord "' + chordName + '" as ' + chordsToTest[chordName], function() {
            var type = new Music.ChordType(chordName);
            var pitch = new Music.Pitch('C');
            var chord = new Music.Chord(type, pitch);
            expect(chord.getPitches().join(',')).to.equal(chordsToTest[chordName]);
        });
    };
    
    var thereAreUntestedChords = false;
    var definedChords = Music.ChordType.getTypeNames();
    
    for (var i=0; i<definedChords.length; i++) {
        if (typeof(chordsToTest[definedChords[i]]) == 'undefined') {
            thereAreUntestedChords = true;
        }
    };
    
    it('does not defined untested chords', function() {
        expect(thereAreUntestedChords).to.be.false;
    });
});