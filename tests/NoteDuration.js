var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.NoteDuration', function() {
    it('NoteDuration exists', function() {
        expect(Music.NoteDuration).to.exist;
    });
    
    it('a quarter note has 96 ticks', function() {
        var note = new Music.NoteDuration(1, 4);
        expect(note.getTicks()).to.equal(96);
        
        var note = new Music.NoteDuration(96);
        expect(note.getTicks()).to.equal(96);
    });
    
    it('8th note triplet is 32 ticks', function() {
        var note = new Music.NoteDuration(1, 8, true);
        expect(note.getTicks()).to.equal(32);
    });
    
    it('quantize itself into a quarter note and eighth', function() {
        var note = new Music.NoteDuration(144);
        var notes = note.quantize();
        expect(notes.length).to.equal(2);
        expect(notes[0].getTicks()).to.equal(96);
        expect(notes[1].getTicks()).to.equal(48);
    });
    
    it('quantize itself seven 8th triplets', function() {
        var note = new Music.NoteDuration(224);
        var notes = note.quantize();
        expect(notes.length).to.equal(2);
        expect(notes[0].getTicks()).to.equal(192);
        expect(notes[1].getTicks()).to.equal(32);
    });
    
    it('quantize seven quarter notes', function() {
        var note = new Music.NoteDuration(672);
        var notes = note.quantize();
        expect(notes.length).to.equal(3);
        expect(notes[0].getTicks()).to.equal(384);
        expect(notes[1].getTicks()).to.equal(192);
        expect(notes[2].getTicks()).to.equal(96);
    });
    
    it('quantize seven eighth notes', function() {
        var note = new Music.NoteDuration(336);
        var notes = note.quantize();
        expect(notes.length).to.equal(3);
        expect(notes[0].getTicks()).to.equal(192);
        expect(notes[1].getTicks()).to.equal(96);
        expect(notes[2].getTicks()).to.equal(48);
    });
});