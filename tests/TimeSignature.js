var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var Music = require('../music.js');

describe('Music.TimeSignature', function() {
    it('TimeSignature exists', function() {
        expect(Music.TimeSignature).to.exist;
    });
    
    it('toString works', function() {
        expect('' + new Music.TimeSignature(3, 4)).to.equal('3/4');
    });
});