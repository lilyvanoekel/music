var Music;
(function (Music) {
    class Bpm {
        constructor(bpm, shuffle = false) {
            this.bpm = bpm;
            this.shuffle = shuffle;
        }
    }
    Music.Bpm = Bpm;
})(Music || (Music = {}));
var module = module;
module.exports = Music;
var Music;
(function (Music) {
    class Chord {
        constructor(type, pitch) {
            this.type = type;
            this.pitch = pitch;
        }
        getPitches() {
            let pitches = [];
            for (let semitone of this.type.getSemitones()) {
                pitches.push(new Music.Pitch(this.pitch.getValue() + semitone));
            }
            return pitches;
        }
        getName() {
            return this.type.getName();
        }
        toString() {
            return this.pitch.getLetter() + this.type.getSymbol();
        }
    }
    Music.Chord = Chord;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class ProgressionItem {
        constructor(chord, duration) {
            this.chord = chord;
            this.duration = duration;
        }
    }
    class ChordProgression {
        constructor() {
            this.length = 0;
        }
        add(chord, duration) {
            this[this.length] = new ProgressionItem(chord, duration);
            this.length++;
        }
    }
    Music.ChordProgression = ChordProgression;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class ChordType {
        constructor(type) {
            this.type = type;
            if (typeof (Music.ChordType.chordTypes[type]) == 'undefined') {
                throw new Error('Unsupported chord type');
            }
        }
        static getTypeNames() {
            return Music.Util.getObjectKeys(Music.ChordType.chordTypes);
        }
        getSemitones() {
            return Music.ChordType.chordTypes[this.type].semitones;
        }
        getSymbol() {
            return Music.ChordType.chordTypes[this.type].symbol;
        }
        getName() {
            return this.type;
        }
    }
    ChordType.chordTypes = {
        major: { semitones: [0, 4, 7], symbol: '' },
        minor: { semitones: [0, 3, 7], symbol: 'm' },
        diminished: { semitones: [0, 3, 6], symbol: 'dim' },
        augmented: { semitones: [0, 4, 8], symbol: 'aug' },
        suspended4: { semitones: [0, 5, 7], symbol: 'sus4' },
        major7: { semitones: [0, 4, 7, 11], symbol: 'maj7' },
        minor7: { semitones: [0, 3, 7, 10], symbol: 'm7' },
        dominant7: { semitones: [0, 4, 7, 10], symbol: '7' },
        diminished7: { semitones: [0, 3, 6, 9], symbol: 'dim7' },
        augmented_major7: { semitones: [0, 4, 8, 11], symbol: 'aug7' },
        augmented_minor7: { semitones: [0, 4, 8, 10], symbol: 'maug7' },
        half_diminished7: { semitones: [0, 3, 6, 10], symbol: 'm7b5' }
    };
    Music.ChordType = ChordType;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class Note {
        constructor(pitch, duration, velocity = 100) {
            this.pitch = pitch;
            this.duration = duration;
            this.velocity = velocity;
        }
    }
    Music.Note = Note;
})(Music || (Music = {}));
var Music;
(function (Music) {
    Music.ticksPerQuarter = 96;
    class NoteDuration {
        constructor(arg1, arg2 = null, isTriplet = false) {
            if (!arg2) {
                this.ticks = arg1;
                return;
            }
            this.ticks = ((4 * Music.ticksPerQuarter) / arg2) * arg1;
            if (isTriplet) {
                this.ticks = this.ticks * 2 / 3;
            }
        }
        getTicks() {
            return this.ticks;
        }
        quantize() {
            let noteTypes = [1, 2, 4, 8, 16, 32, 64];
            let ticksPerWhole = 4 * Music.ticksPerQuarter;
            let validTicks = [];
            let smallestValue = 0;
            let notes = [];
            for (let type of noteTypes) {
                let ticksPerCurrent = Math.floor(ticksPerWhole / type);
                let ticksPerCurrentTriplet = Math.floor(ticksPerCurrent * 2 / 3);
                validTicks.push(ticksPerCurrent);
                if (type > 2) {
                    validTicks.push(ticksPerCurrentTriplet);
                }
                smallestValue = ticksPerCurrentTriplet;
            }
            let ticksLeft = this.getTicks();
            let matchesEntireNote = function () {
                for (let ticks of validTicks) {
                    if (ticksLeft == ticks) {
                        return ticks;
                    }
                }
                return null;
            };
            while (ticksLeft > (smallestValue - 1)) {
                if (ticksLeft >= ticksPerWhole) {
                    notes.push(new Music.NoteDuration(ticksPerWhole));
                    ticksLeft -= ticksPerWhole;
                    continue;
                }
                let entireNoteTicks = matchesEntireNote();
                if (entireNoteTicks) {
                    notes.push(new Music.NoteDuration(entireNoteTicks));
                    break;
                }
                let halfTicksLeft = Math.floor(ticksLeft / 2);
                let halfTicksMatched = function () {
                    for (let i = validTicks.length - 1; i >= 0; i--) {
                        if (validTicks[i] >= halfTicksLeft) {
                            return validTicks[i];
                        }
                    }
                    return null;
                }();
                if (!halfTicksMatched) {
                    break;
                }
                notes.push(new Music.NoteDuration(halfTicksMatched));
                ticksLeft -= halfTicksMatched;
            }
            return notes;
        }
    }
    Music.NoteDuration = NoteDuration;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class NoteEvent {
        constructor(type, note) {
            this.type = type;
            this.note = note;
        }
    }
    Music.NoteEvent = NoteEvent;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class PhraseItem {
        constructor(duration, notes) {
            this.duration = duration;
            this.notes = notes;
        }
    }
    class Phrase {
        constructor(timeSignature) {
            this.timeSignature = timeSignature;
            this.length = 0;
        }
        add(duration, notes) {
            this[this.length] = new PhraseItem(duration, (notes ? notes : []));
            this.length++;
        }
        getNoteEvents() {
            let eventsByTime = {};
            let events = [];
            let currentTicks = 0;
            for (let i = 0; i < this.length; i++) {
                let item = this[i];
                for (let j = 0; j < item.notes.length; j++) {
                    let note = item.notes[j];
                    if (typeof (eventsByTime[currentTicks]) == 'undefined') {
                        eventsByTime[currentTicks] = [];
                    }
                    if (typeof (eventsByTime[currentTicks + note.duration.getTicks()]) == 'undefined') {
                        eventsByTime[currentTicks + note.duration.getTicks()] = [];
                    }
                    eventsByTime[currentTicks].push(new Music.NoteEvent('on', note));
                    eventsByTime[currentTicks + note.duration.getTicks()].push(new Music.NoteEvent('off', note));
                }
                currentTicks += item.duration.getTicks();
            }
            let previousTicks = 0;
            for (let ticks in eventsByTime) {
                let ticksNumber = parseInt(ticks, 10);
                for (let i = 0; i < eventsByTime[ticks].length; i++) {
                    let e = eventsByTime[ticks][i];
                    e.delta = (i == 0 ? ticksNumber - previousTicks : 0);
                    events.push(e);
                }
                previousTicks = ticksNumber;
            }
            return events;
        }
    }
    Music.Phrase = Phrase;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class Pitch {
        constructor(pitchValue, octave = 0) {
            if (isNaN(pitchValue)) {
                this.letter = pitchValue;
                this.value = Music.Pitch.letters.indexOf(this.letter) + (octave * 12);
            }
            else {
                this.value = pitchValue;
                this.letter = Music.Pitch.letters[this.value % 12];
            }
        }
        getLetter() {
            return this.letter;
        }
        getValue() {
            return this.value;
        }
        toString() {
            return this.getLetter();
        }
    }
    Pitch.letters = [
        'C',
        'C#',
        'D',
        'D#',
        'E',
        'F',
        'F#',
        'G',
        'G#',
        'A',
        'A#',
        'B'
    ];
    Music.Pitch = Pitch;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class RhythmItem {
        constructor(size, soundFor) {
            this.size = size;
            this.soundFor = soundFor;
        }
    }
    class Rhythm {
        constructor(timeSignature) {
            this.timeSignature = timeSignature;
            this.length = 0;
        }
        add(size, soundFor) {
            this[this.length] = new RhythmItem(size, (soundFor ? soundFor : size));
            this.length++;
        }
    }
    Music.Rhythm = Rhythm;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class Scale {
        constructor(type, pitch) {
            this.type = type;
            this.pitch = pitch;
        }
        getPitches() {
            let pitches = [];
            for (let semitone of this.type.getSemitones()) {
                pitches.push(new Music.Pitch(this.pitch.getValue() + semitone));
            }
            return pitches;
        }
        getPitchByNumeral(numeral) {
            numeral = (parseInt(numeral, 10) - 1) % 7;
            return new Music.Pitch(this.pitch.getValue() + this.type.getSemitones()[numeral]);
        }
    }
    Music.Scale = Scale;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class ScaleType {
        constructor(type) {
            this.type = type;
            if (typeof (Music.ScaleType.types[type]) == 'undefined') {
                throw new Error('Unsupported scale type');
            }
        }
        getSemitones() {
            return Music.ScaleType.types[this.type].semitones;
        }
        getName() {
            return this.type;
        }
        static getTypes() {
            return Music.Util.getObjectKeys(Music.ScaleType.types);
        }
    }
    ScaleType.types = {
        major: { semitones: [0, 2, 4, 5, 7, 9, 11] },
        minor: { semitones: [0, 2, 3, 5, 7, 8, 10] },
        minor_harmonic: { semitones: [0, 2, 3, 5, 7, 8, 11] }
    };
    Music.ScaleType = ScaleType;
})(Music || (Music = {}));
var Music;
(function (Music) {
    class TimeSignature {
        constructor(beatsPerMeasure, beatLength) {
            this.beatsPerMeasure = beatsPerMeasure;
            this.beatLength = beatLength;
        }
        toString() {
            return this.beatsPerMeasure + '/' + this.beatLength;
        }
    }
    Music.TimeSignature = TimeSignature;
})(Music || (Music = {}));
var Music;
(function (Music) {
    var Generator;
    (function (Generator) {
        class Abstract {
            constructor(random) {
                this.random = random;
                if (!random) {
                    throw new Error('Random number generator is required');
                }
            }
        }
        Generator.Abstract = Abstract;
    })(Generator = Music.Generator || (Music.Generator = {}));
})(Music || (Music = {}));
var Music;
(function (Music) {
    var Generator;
    (function (Generator) {
        class Bpm extends Music.Generator.Abstract {
            constructor() {
                super(...arguments);
                this._min = 80;
                this._max = 180;
            }
            set min(min) {
                this._min = min;
            }
            set max(max) {
                this._max = max;
            }
            generate() {
                return new Music.Bpm(this.random.getNumberBetween(this._min, this._max));
            }
        }
        Generator.Bpm = Bpm;
    })(Generator = Music.Generator || (Music.Generator = {}));
})(Music || (Music = {}));
var Music;
(function (Music) {
    var Generator;
    (function (Generator) {
        class Scale extends Music.Generator.Abstract {
            constructor() {
                super(...arguments);
                this._types = [];
                this._pitch = [];
            }
            set types(types) {
                if (Array.isArray(types)) {
                    this._types = types;
                }
                else {
                    this._types = [types];
                }
            }
            set pitch(pitch) {
                if (Array.isArray(pitch)) {
                    this._pitch = pitch;
                }
                else {
                    this._pitch = [pitch];
                }
            }
            generate() {
                let scaleType;
                let pitch;
                if (this._types.length) {
                    scaleType = new Music.ScaleType(this.random.arrayRandomChild(this._types));
                }
                else {
                    let scaleTypes = Music.ScaleType.getTypes();
                    scaleType = new Music.ScaleType(this.random.arrayRandomChild(scaleTypes));
                }
                if (this._pitch.length) {
                    pitch = new Music.Pitch(this.random.arrayRandomChild(this._pitch));
                }
                else {
                    pitch = new Music.Pitch(this.random.getNumberBetween(0, 11));
                }
                return new Music.Scale(scaleType, pitch);
            }
        }
        Generator.Scale = Scale;
    })(Generator = Music.Generator || (Music.Generator = {}));
})(Music || (Music = {}));
var Music;
(function (Music) {
    var Generator;
    (function (Generator) {
        var Phrase;
        (function (Phrase) {
            class SimpleChords {
            }
            Phrase.SimpleChords = SimpleChords;
        })(Phrase = Generator.Phrase || (Generator.Phrase = {}));
    })(Generator = Music.Generator || (Music.Generator = {}));
})(Music || (Music = {}));
var Music;
(function (Music) {
    var Generator;
    (function (Generator) {
        var Phrase;
        (function (Phrase) {
            class SimpleDrums {
            }
            Phrase.SimpleDrums = SimpleDrums;
        })(Phrase = Generator.Phrase || (Generator.Phrase = {}));
    })(Generator = Music.Generator || (Music.Generator = {}));
})(Music || (Music = {}));
var Music;
(function (Music) {
    var Generator;
    (function (Generator) {
        var Progression;
        (function (Progression) {
            class PredefinedFourBars extends Music.Generator.Abstract {
                generate() {
                    if (!this.scale) {
                        throw new Error('Scale is required');
                    }
                    let progression = new Music.ChordProgression();
                    let data = this._getChordData();
                    let chordToDouble = null;
                    if (data.length == 3) {
                        chordToDouble = this.random.getNumberBetween(0, 2);
                    }
                    for (let i = 0; i < data.length; i++) {
                        let chord = new Music.Chord(new Music.ChordType(data[i][1]), this.scale.getPitchByNumeral(data[i][0]));
                        progression.add(chord, new Music.NoteDuration(1, 1));
                        if (i == chordToDouble) {
                            progression.add(chord, new Music.NoteDuration(1, 1));
                        }
                    }
                    return progression;
                }
                _getChordData() {
                    return this.random.arrayRandomChild(Music.Generator.Progression.PredefinedFourBars._data[this.scale.type.getName()]);
                }
            }
            PredefinedFourBars._data = {
                major: [
                    [['1', 'major'], ['4', 'major'], ['5', 'major']],
                    [['1', 'major'], ['6', 'minor'], ['4', 'major'], ['5', 'major']],
                    [['2', 'minor7'], ['5', 'dominant7'], ['1', 'major7']],
                    [['1', 'major'], ['6', 'minor'], ['2', 'minor'], ['5', 'major']],
                    [['1', 'major'], ['5', 'major'], ['6', 'minor'], ['4', 'major']],
                    [['1', 'major'], ['4', 'major'], ['6', 'minor'], ['5', 'major']],
                    [['1', 'major'], ['3', 'minor'], ['4', 'major'], ['5', 'major']],
                    [['1', 'major'], ['4', 'major'], ['1', 'major'], ['5', 'major']],
                    [['1', 'major'], ['4', 'major'], ['2', 'minor'], ['5', 'major']]
                ],
                minor: [
                    [['1', 'minor'], ['6', 'major'], ['7', 'major']],
                    [['1', 'minor'], ['4', 'minor'], ['7', 'major']],
                    [['1', 'minor'], ['4', 'minor'], ['5', 'minor']],
                    [['1', 'minor'], ['6', 'major'], ['3', 'major'], ['7', 'major']],
                    [['2', 'half_diminished7'], ['5', 'minor'], ['1', 'minor']],
                    [['1', 'minor'], ['4', 'minor'], ['5', 'minor'], ['1', 'minor']],
                    [['6', 'major'], ['7', 'major'], ['1', 'minor'], ['1', 'minor']],
                    [['1', 'minor'], ['7', 'major'], ['6', 'major'], ['7', 'major']],
                    [['1', 'minor'], ['4', 'minor'], ['1', 'minor']]
                ]
            };
            Progression.PredefinedFourBars = PredefinedFourBars;
        })(Progression = Generator.Progression || (Generator.Progression = {}));
    })(Generator = Music.Generator || (Music.Generator = {}));
})(Music || (Music = {}));
var seedrandom = require('seedrandom');
var Music;
(function (Music) {
    var Util;
    (function (Util) {
        class Random {
            constructor(seed) {
                this.seed = seed;
                this.rng = seedrandom(seed);
            }
            getNumber() {
                return this.rng();
            }
            getNumberBetween(start, end) {
                return start + Math.floor(this.getNumber() * (end - start));
            }
            arrayRandomChild(array) {
                return array[Math.floor(this.getNumber() * array.length)];
            }
            objectRandomPropertyKey(object) {
                var keys = Music.Util.getObjectKeys(object);
                return this.arrayRandomChild(keys);
            }
            arrayAssignRandom(array, value) {
                array[Math.floor(this.getNumber() * array.length)] = value;
                return array;
            }
        }
        Util.Random = Random;
    })(Util = Music.Util || (Music.Util = {}));
})(Music || (Music = {}));
var Music;
(function (Music) {
    var Util;
    (function (Util) {
        function getObjectKeys(object) {
            var keys = [];
            for (var x in object) {
                keys.push(x);
            }
            return keys;
        }
        Util.getObjectKeys = getObjectKeys;
    })(Util = Music.Util || (Music.Util = {}));
})(Music || (Music = {}));
