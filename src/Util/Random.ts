declare function require(name:string);
var seedrandom = require('seedrandom');

namespace Music {
    export namespace Util {
        export class Random {
            rng : any;
            
            constructor(public seed: string) {
                this.rng = seedrandom(seed);
            }
            
            getNumber() : number {
                return this.rng();
            }
            
            getNumberBetween(start : number, end : number) : number {
                return start + Math.floor(this.getNumber() * (end - start));
            }
            
            arrayRandomChild(array) {
                return array[Math.floor(this.getNumber() * array.length)];
            }
            
            objectRandomPropertyKey(object) : string {
                var keys = Music.Util.getObjectKeys(object);
                return this.arrayRandomChild(keys);
            }
            
            arrayAssignRandom(array, value) : void {
                array[Math.floor(this.getNumber() * array.length)] = value;
                return array;
            }
        }
    }
}