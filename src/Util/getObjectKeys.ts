namespace Music {
    export namespace Util {
        export function getObjectKeys(object) {
            var keys = [];
            
            for (var x in object) {
                keys.push(x);
            }
            
            return keys;
        }
    }
}