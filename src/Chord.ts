namespace Music {
    export class Chord {
        constructor(public type : Music.ChordType, public pitch : Music.Pitch) {
            
        }
        
        getPitches() : Array<Music.Pitch> {
            let pitches = [];
            
            for (let semitone of this.type.getSemitones()) {
                pitches.push(new Music.Pitch(this.pitch.getValue() + semitone));
            }
            
            return pitches;
        }
        
        getName() : string {
            return this.type.getName();
        }
        
        toString() : string {
            return this.pitch.getLetter() + this.type.getSymbol();
        }
    }
}