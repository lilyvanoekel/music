namespace Music {
    export class Pitch {
        static letters = [
            'C',
            'C#',
            'D',
            'D#',
            'E',
            'F',
            'F#',
            'G',
            'G#',
            'A',
            'A#',
            'B'
        ];
        
        private letter : string;
        private value : number;
        
        constructor(pitchValue, octave : number = 0) {
            if (isNaN(pitchValue)) {
                this.letter = pitchValue;
                this.value = Music.Pitch.letters.indexOf(this.letter) + (octave * 12);
            } else {
                this.value = pitchValue;
                this.letter = Music.Pitch.letters[this.value % 12];
            }
        }
        
        getLetter() : string {
            return this.letter;
        }
        
        getValue() : number {
            return this.value;
        }
        
        toString() : string {
            return this.getLetter();
        }
    }
}