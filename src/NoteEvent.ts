namespace Music {
    export class NoteEvent {
        public delta : number;
        
        constructor(public type : string, public note : Music.Note) {
            
        }
    }
}