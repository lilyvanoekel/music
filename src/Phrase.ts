namespace Music {
    class PhraseItem {
        constructor(public duration : Music.NoteDuration, public notes : Array<Music.Note>) {
            
        }
    }
    
    export class Phrase {
        public length = 0;
        
        constructor(public timeSignature : Music.TimeSignature) {
            
        }
        
        add(duration : Music.NoteDuration, notes : Array<Music.Note>) : void {
            this[this.length] = new PhraseItem(duration, (notes ? notes : []));
            this.length++;
        }
    
        getNoteEvents() : Array<NoteEvent> {
            let eventsByTime = {};
            let events : Array<NoteEvent> = [];
            let currentTicks = 0;
            
            for (let i=0; i<this.length; i++) {
                let item = this[i];
                
                for (let j=0; j<item.notes.length; j++) {
                    let note = item.notes[j];
                    
                    if (typeof(eventsByTime[currentTicks]) == 'undefined') {
                        eventsByTime[currentTicks] = [];
                    }
                    
                    if (typeof(eventsByTime[currentTicks + note.duration.getTicks()]) == 'undefined') {
                        eventsByTime[currentTicks + note.duration.getTicks()] = [];
                    }
                    
                    eventsByTime[currentTicks].push(new Music.NoteEvent('on', note));
                    eventsByTime[currentTicks + note.duration.getTicks()].push(new Music.NoteEvent('off', note));
                }
                
                currentTicks += item.duration.getTicks();
            }
            
            let previousTicks : number = 0;
            for (let ticks in eventsByTime) {
                let ticksNumber = parseInt(ticks, 10);
                
                for (let i=0; i<eventsByTime[ticks].length; i++) {
                    let e = eventsByTime[ticks][i];
                    e.delta = (i == 0 ? ticksNumber - previousTicks : 0);
                    events.push(e);
                }
                
                previousTicks = ticksNumber;
            }
            
            return events;
        }
    }
}