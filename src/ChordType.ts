namespace Music {
    export class ChordType {
        private static chordTypes = {
            major:              {semitones: [0, 4, 7],      symbol: '' },
            minor:              {semitones: [0, 3, 7],      symbol: 'm' },
            diminished:         {semitones: [0, 3, 6],      symbol: 'dim'  },
            augmented:          {semitones: [0, 4, 8],      symbol: 'aug'  },
            suspended4:         {semitones: [0, 5, 7],      symbol: 'sus4'  },
            major7:             {semitones: [0, 4, 7, 11],  symbol: 'maj7'  },
            minor7:             {semitones: [0, 3, 7, 10],  symbol: 'm7'  },
            dominant7:          {semitones: [0, 4, 7, 10],  symbol: '7'  },
            diminished7:        {semitones: [0, 3, 6, 9],   symbol: 'dim7'  },
            augmented_major7:   {semitones: [0, 4, 8, 11],  symbol: 'aug7'  },
            augmented_minor7:   {semitones: [0, 4, 8, 10],  symbol: 'maug7'  },
            half_diminished7:   {semitones: [0, 3, 6, 10],  symbol: 'm7b5'  }
        };
        
        static getTypeNames() : Array<string> {
            return Music.Util.getObjectKeys(Music.ChordType.chordTypes);
        }
        
        constructor(private type : string) {
            if (typeof(Music.ChordType.chordTypes[type]) == 'undefined') {
                throw new Error('Unsupported chord type');
            }
        }
        
        getSemitones() : Array<number> {
            return Music.ChordType.chordTypes[this.type].semitones;
        }
        
        getSymbol() : string {
            return Music.ChordType.chordTypes[this.type].symbol;
        }
        
        getName() : string {
            return this.type;
        }
    }
}