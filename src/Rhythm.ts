namespace Music {
    class RhythmItem {
        constructor(public size : Music.NoteDuration, public soundFor : Music.NoteDuration) {
            
        }
    }
    
    export class Rhythm {
        public length = 0;
        
        constructor(public timeSignature : Music.TimeSignature) {
            
        }
        
        add(size : Music.NoteDuration, soundFor : Music.NoteDuration) : void {
            this[this.length] = new RhythmItem(size, (soundFor ? soundFor : size));
            this.length++;
        }
    }
}