namespace Music {
    export class Scale {
        constructor(public type : Music.ScaleType, public pitch : Music.Pitch) {
            
        }
        
        getPitches() : Array<Music.Pitch> {
            let pitches = [];
            
            for (let semitone of this.type.getSemitones()) {
                pitches.push(new Music.Pitch(this.pitch.getValue() + semitone));
            }
            
            return pitches;
        }
        
        getPitchByNumeral(numeral) : Music.Pitch {
            numeral = (parseInt(numeral, 10) - 1) % 7;
            return new Music.Pitch(this.pitch.getValue() + this.type.getSemitones()[numeral]);
        }
    }
}