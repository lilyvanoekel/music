namespace Music {
    export class Note {
        constructor(public pitch : Music.Pitch, public duration : Music.NoteDuration, public velocity : number = 100) {
            
        }
    }
}