namespace Music {
    class ProgressionItem {
        constructor(public chord : Music.Chord, public duration : Music.NoteDuration) {
            
        }
    }
    
    export class ChordProgression {
        public length = 0;
        
        add(chord : Music.Chord, duration : Music.NoteDuration) : void {
            this[this.length] = new ProgressionItem(chord, duration);
            this.length++;
        }
    }
}