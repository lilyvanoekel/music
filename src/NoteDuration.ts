namespace Music {
    export const ticksPerQuarter = 96;
    
    export class NoteDuration {
        private ticks : number;
        
        constructor(arg1, arg2 : number = null, isTriplet : boolean = false) {
            if (!arg2) {
                this.ticks = arg1;
                return;
            }
            
            this.ticks = ((4 * Music.ticksPerQuarter) / arg2) * arg1;
            
            if (isTriplet) {
                this.ticks = this.ticks * 2 / 3;
            }
        }
        
        getTicks() : number {
            return this.ticks;
        }
        
        quantize() : Array<NoteDuration> {
            let noteTypes = [1, 2, 4, 8, 16, 32, 64];
            let ticksPerWhole = 4 * Music.ticksPerQuarter;
            let validTicks = [];
            let smallestValue = 0;
            let notes = [];
            
            for (let type of noteTypes) {
                let ticksPerCurrent = Math.floor(ticksPerWhole / type);
                let ticksPerCurrentTriplet = Math.floor(ticksPerCurrent * 2 / 3);
                validTicks.push(ticksPerCurrent);
                
                if (type > 2) {
                    validTicks.push(ticksPerCurrentTriplet);
                }
                
                smallestValue = ticksPerCurrentTriplet;
            }
            
            let ticksLeft = this.getTicks();
            
            let matchesEntireNote = function() {
                for (let ticks of validTicks) {
                    if (ticksLeft == ticks) {
                        return ticks;
                    }
                }
                
                return null;
            };
            
            while (ticksLeft > (smallestValue - 1)) {
                if (ticksLeft >= ticksPerWhole) {
                    notes.push(new Music.NoteDuration(ticksPerWhole));
                    ticksLeft -= ticksPerWhole;
                    continue;
                }
                
                let entireNoteTicks = matchesEntireNote();
                
                if (entireNoteTicks) {
                    notes.push(new Music.NoteDuration(entireNoteTicks));
                    break;
                }
                
                let halfTicksLeft = Math.floor(ticksLeft / 2);
                let halfTicksMatched = function() {
                    for (let i=validTicks.length - 1; i >= 0; i--) {
                        if (validTicks[i] >= halfTicksLeft) {
                            return validTicks[i];
                        }
                    }
                    
                    return null;
                }();
                
                if (!halfTicksMatched) {
                    break;
                }
                
                notes.push(new Music.NoteDuration(halfTicksMatched));
                ticksLeft -= halfTicksMatched;
            }
            
            return notes;
        }
    }
}