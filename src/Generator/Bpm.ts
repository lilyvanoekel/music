namespace Music {
    export namespace Generator {
        export class Bpm extends Music.Generator.Abstract {
            private _min : number = 80;
            private _max : number = 180;
            
            set min(min : number) {
                this._min = min;
            }
            
            set max(max : number) {
                this._max = max;
            }
            
            generate() {
                return new Music.Bpm(this.random.getNumberBetween(this._min, this._max));
            }
        }
    }
}