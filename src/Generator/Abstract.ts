namespace Music {
    export namespace Generator {
        export abstract class Abstract {
            constructor(public random : Music.Util.Random) {
                if (!random) {
                    throw new Error('Random number generator is required');
                }
            }
            
            abstract generate();
        }
    }
}