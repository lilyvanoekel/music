namespace Music {
    export namespace Generator {
        export class Scale extends Music.Generator.Abstract {
            protected _types : Array<string> = [];
            protected _pitch : Array<string> = [];
            
            set types(types) {
                if (Array.isArray(types)) {
                    this._types = types;
                } else {
                    this._types = [types];
                }
            }
            
            set pitch(pitch) {
                if (Array.isArray(pitch)) {
                    this._pitch = pitch;
                } else {
                    this._pitch = [pitch];
                }
            }
            
            generate() {
                let scaleType;
                let pitch;
                
                if (this._types.length) {
                    scaleType = new Music.ScaleType(this.random.arrayRandomChild(this._types));
                } else {
                    let scaleTypes = Music.ScaleType.getTypes();
                    scaleType = new Music.ScaleType(this.random.arrayRandomChild(scaleTypes));
                }
                
                if (this._pitch.length) {
                    pitch = new Music.Pitch(this.random.arrayRandomChild(this._pitch));
                } else {
                    pitch = new Music.Pitch(this.random.getNumberBetween(0, 11));
                }
                
                return new Music.Scale(scaleType, pitch);
            }
        }
    }
}