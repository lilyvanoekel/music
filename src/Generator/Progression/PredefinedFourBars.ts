namespace Music {
    export namespace Generator {
        export namespace Progression {
            export class PredefinedFourBars extends Music.Generator.Abstract {
                private static _data = {
                    major: [
                        [['1', 'major'],    ['4', 'major'],     ['5', 'major']],
                        [['1', 'major'],    ['6', 'minor'],     ['4', 'major'],     ['5', 'major']],
                        [['2', 'minor7'],   ['5', 'dominant7'], ['1', 'major7']],
                        [['1', 'major'],    ['6', 'minor'],     ['2', 'minor'],     ['5', 'major']],
                        [['1', 'major'],    ['5', 'major'],     ['6', 'minor'],     ['4', 'major']],
                        [['1', 'major'],    ['4', 'major'],     ['6', 'minor'],     ['5', 'major']],
                        [['1', 'major'],    ['3', 'minor'],     ['4', 'major'],     ['5', 'major']],
                        [['1', 'major'],    ['4', 'major'],     ['1', 'major'],     ['5', 'major']],
                        [['1', 'major'],    ['4', 'major'],     ['2', 'minor'],     ['5', 'major']]
                    ],
                    minor: [
                        [['1', 'minor'],    ['6', 'major'],     ['7', 'major']],
                        [['1', 'minor'],    ['4', 'minor'],     ['7', 'major']],
                        [['1', 'minor'],    ['4', 'minor'],     ['5', 'minor']],
                        [['1', 'minor'],    ['6', 'major'],     ['3', 'major'],     ['7', 'major']],
                        [['2', 'half_diminished7'], ['5', 'minor'], ['1', 'minor']],
                        [['1', 'minor'],    ['4', 'minor'],     ['5', 'minor'],     ['1', 'minor']],
                        [['6', 'major'],    ['7', 'major'],     ['1', 'minor'],     ['1', 'minor']],
                        [['1', 'minor'],    ['7', 'major'],     ['6', 'major'],     ['7', 'major']],
                        [['1', 'minor'],    ['4', 'minor'],     ['1', 'minor']]
                    ]
                };
                
                public scale : Music.Scale;
                
                generate() {
                    if (!this.scale) {
                        throw new Error('Scale is required');
                    }
                    
                    let progression = new Music.ChordProgression();
                    let data = this._getChordData();
                    let chordToDouble : number = null;
                    
                    if (data.length == 3) {
                        chordToDouble = this.random.getNumberBetween(0, 2);
                    }
                    
                    for (let i=0; i<data.length; i++) {
                        let chord = new Music.Chord(
                            new Music.ChordType(data[i][1]),
                            this.scale.getPitchByNumeral(data[i][0])
                        );
                        
                        progression.add(chord, new Music.NoteDuration(1, 1));
                        
                        if (i == chordToDouble) {
                            progression.add(chord, new Music.NoteDuration(1, 1));
                        }
                    }
                    
                    return progression;
                }
                
                private _getChordData() {
                    return this.random.arrayRandomChild(
                        Music.Generator.Progression.PredefinedFourBars._data[this.scale.type.getName()]
                    );
                }
            }
        }
    }
}