namespace Music {
    export class ScaleType {
        private static types = {
            major: {            semitones: [0, 2, 4, 5, 7, 9, 11]   },
            minor: {            semitones: [0, 2, 3, 5, 7, 8, 10]   },
            minor_harmonic: {   semitones: [0, 2, 3, 5, 7, 8, 11]   }
        };
        
        constructor(private type : string) {
            if (typeof(Music.ScaleType.types[type]) == 'undefined') {
                throw new Error('Unsupported scale type');
            }
        }
        
        getSemitones() : Array<number> {
            return Music.ScaleType.types[this.type].semitones;
        }
        
        getName() : string {
            return this.type;
        }
        
        static getTypes() : Array<string> {
            return Music.Util.getObjectKeys(Music.ScaleType.types);
        }
    }
}