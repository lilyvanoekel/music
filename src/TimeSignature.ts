namespace Music {
    export class TimeSignature {
        constructor(public beatsPerMeasure : number, public beatLength : number) {
            
        }
        
        toString() : string {
            return this.beatsPerMeasure + '/' + this.beatLength;
        }
    }
}