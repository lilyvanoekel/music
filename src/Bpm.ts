namespace Music {
    export class Bpm {
        constructor(public bpm : number, public shuffle : boolean = false) {
            
        }
    }
}

var module: any = <any>module;
module.exports = Music;